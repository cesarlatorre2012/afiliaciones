/** 
 * 
 */

function retornarFecha()
{
  var fecha
  fecha=new Date();
  var cadena=' Dia ' + fecha.getDate() + ' del mes ' + (fecha.getMonth()+1) + ' del año ' + fecha.getFullYear();
  return cadena;
}

function retornarHora()
{
  var fecha
  fecha=new Date();
  var cadena='Hora: ' + fecha.getHours()+':'+fecha.getMinutes()+':'+fecha.getSeconds();
  return cadena; 
}