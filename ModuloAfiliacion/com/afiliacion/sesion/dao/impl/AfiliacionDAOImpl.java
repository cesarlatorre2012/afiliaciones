package com.afiliacion.sesion.dao.impl;

import java.io.Serializable;
import java.sql.SQLException;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import com.afiliacion.sesion.dao.AfiliacionDAO;
import com.smemax.persistence.utils.StoredProcedureExecutor;

@SuppressWarnings("serial")
public class AfiliacionDAOImpl extends JdbcDaoSupport implements AfiliacionDAO, Serializable {

	@Override
	public String afiliacion(String tipoIdentificacion, String numeroIdentificacion, String email, String celular)
			throws SQLException {

		StoredProcedureExecutor executor = null;
		String mensaje = null;
		
		try{
			
			String sql = "CREAR_AFILACION";
			
			executor = new StoredProcedureExecutor( sql, getDataSource() );
			executor.addInputParameter( "P_TIPO_IDENTIFICACION", OracleTypes.VARCHAR, tipoIdentificacion );
			executor.addInputParameter( "P_NUMERO_IDENTIFICACION", OracleTypes.VARCHAR, numeroIdentificacion );
			executor.addInputParameter( "P_EMAIL", OracleTypes.VARCHAR, email );
			executor.addInputParameter( "P_TELEFONO", OracleTypes.VARCHAR, celular );
			executor.addOutputParameter( "MSGTEXTERROR", OracleTypes.VARCHAR, "");
				
			executor.executeProc();
			
			mensaje = (String) executor.getParameter("MSGTEXTERROR");
								
		}catch( Exception e ) {
			e.printStackTrace();
		
		}finally{
			executor = null;
		}
		
		return mensaje;				
	}

}
