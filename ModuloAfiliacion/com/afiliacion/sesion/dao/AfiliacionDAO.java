package com.afiliacion.sesion.dao;

import java.sql.SQLException;

public interface AfiliacionDAO {
	public String afiliacion(String tipoIdentificacion, String numeroIdentificacion,
            String email, String celular ) throws SQLException;
}
