package com.afiliacion.sesion.servicio;

import java.sql.SQLException;

public interface SesionAfiliacion {
	public String afiliacion(String tipoIdentificacion, String numeroIdentificacion,
			                 String email, String celular ) throws SQLException;
}
