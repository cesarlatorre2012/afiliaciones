package com.afiliacion.sesion.servicio.impl;

import java.sql.SQLException;
import com.afiliacion.sesion.dao.AfiliacionDAO;
import com.afiliacion.sesion.servicio.SesionAfiliacion;
import com.smemax.common.service.locator.ServiceLocator;

public class AfiliacionServicioImpl implements SesionAfiliacion {
	
	private AfiliacionDAO afiliacionDAO;
	
	public AfiliacionServicioImpl()throws Exception {
		setUp();
	}
	
	public void setUp() {
		if ( afiliacionDAO == null ) {
			afiliacionDAO = ( AfiliacionDAO ) ServiceLocator.getInstance().getService(
			    "afiliacionDAO" );
		}
    }

	@Override
	public String afiliacion(String tipoIdentificacion, String numeroIdentificacion, String email, String celular)
			throws SQLException {
		return afiliacionDAO.afiliacion(tipoIdentificacion, numeroIdentificacion, email, celular);
	}

}
