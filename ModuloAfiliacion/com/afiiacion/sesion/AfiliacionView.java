package com.afiiacion.sesion;

import java.sql.SQLException;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;

@ManagedBean
public class AfiliacionView {
	
	private String tipoIdentificacion;
	private String identificacion;
	private String correoElectronico; 
	private String celular;
	
	private AfiliacionServicioView afiliacionServicioView;
	

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
	public String getIdentificacion() {
		return identificacion;
	}
	
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	
	public String getCelular() {
		return celular;
	}
	
	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	public void afiliarse(ActionEvent actionEvent) throws SQLException {
		afiliacionServicioView = new AfiliacionServicioView();
		afiliacionServicioView.registrarse(tipoIdentificacion, identificacion, correoElectronico, celular);
	}
	
}
