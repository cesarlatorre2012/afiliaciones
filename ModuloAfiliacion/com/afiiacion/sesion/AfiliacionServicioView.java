package com.afiiacion.sesion;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import com.afiliacion.sesion.servicio.impl.AfiliacionServicioImpl;

public class AfiliacionServicioView {
	
	private AfiliacionServicioImpl service = null;
	private String mensaje;
	
    public void registrarse(String tipoIdentificacion, String identificacion, String correoElectronico,String telefono) {
    	   	
    	try {
			service = new AfiliacionServicioImpl();
			mensaje = service.afiliacion(tipoIdentificacion, identificacion, correoElectronico, telefono);

		} catch (Exception e) {
			
			e.printStackTrace();
		}
    	
    }

}
