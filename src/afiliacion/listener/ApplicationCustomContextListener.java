package afiliacion.listener;

import com.smemax.common.service.locator.ServiceLocator;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationCustomContextListener
  implements ServletContextListener
{
  protected void init()
    throws Exception
  {
    String springPaths = "spring.xml";
    ServiceLocator.setConfigPath(springPaths);
    ServiceLocator.getInstance();
  }
  
  public void contextInitialized(ServletContextEvent paramServletContextEvent) {}
  
  public void contextDestroyed(ServletContextEvent paramServletContextEvent) {}
}
